let url = "https://ajax.test-danit.com/api/swapi/films";
let ulElement = document.querySelector("#listFilms");


const getCharacter = (urlCharacter, idList) => {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", urlCharacter);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send()
    xhr.onload = () => {
        let res = xhr.response;
        res = JSON.parse(res);
        document.querySelector(`#${idList}`).innerHTML += `<li>${res.name}</li>`
    }
}


let generateFilms = (films) => {
    films.forEach((film, index) => {
        ulElement.innerHTML += `
        <li>
            <p class="p1">Episod: ${film.episodeId}</p>
            <p class="p2">${film.name}</p>
            <p class="p3">${film.openingCrawl}</p>
            <img id="loading-${index}" src="Bean%20Eater-1s-200px.gif"/>
            <ul id="character-${index}"></ul>
        </li>`
        film.characters.forEach(character => {
            getCharacter(character, `character-${index}`)
        })
        setTimeout(() => {
            document.querySelector(`#loading-${index}`).style.display = "none"
        }, 2000);

    })
}


function requestAPI() {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send()
    xhr.onload = () => {
        let res = xhr.response;
        if (xhr.status === 200) {
            res = JSON.parse(res);
            generateFilms(res)
            console.log(res)

        } else {
            console.log(res);
        }
    }
}

requestAPI()